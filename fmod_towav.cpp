#include <string>
#include <sstream>
#include <fstream>
#include <iostream>
#include <iomanip>

#include              "fmodex/include/fmod.hpp"
#include              "fmodex/include/fmod_errors.h"
#pragma comment( lib, "fmodex/lib/vs/Win32/fmodex_vc.lib" )

class FMODSystem
{
private:
	FMOD::System * _system;
	FMOD_RESULT    _result;
public:
	inline FMOD::System * operator -> (void)
	{
		return _system;
	}
	inline operator FMOD_RESULT (void) const
	{
		return _result;
	}
	inline operator bool(void) const
	{
		return (_system != 0) && (FMOD_OK == _result);
	}
public:
	inline FMODSystem(void) : _system(0), _result(FMOD_OK)
	{
		_result = FMOD::System_Create(&_system);
	}
	inline ~FMODSystem(void)
	{
		if (operator bool())
		{
			FMOD::System * const system = _system;
			_system = 0;
			if (FMOD_OK == system->close())
				system->release();
		}
	}
};

class FMODSound
{
private:
	FMOD::Sound * _sound;
	FMOD_RESULT   _result;
public:
	inline FMOD::Sound * operator -> (void)
	{
		return _sound;
	}
	inline operator FMOD_RESULT (void) const
	{
		return _result;
	}
	inline operator bool(void) const
	{
		return (_sound != 0) && (FMOD_OK == _result);
	}
public:
	inline FMODSound(FMODSystem & system, const char * name_or_data, FMOD_MODE mode = FMOD_DEFAULT) : _sound(0), _result(FMOD_OK)
	{
		_result = system->createSound(name_or_data, mode, 0, &_sound);
	}
	inline ~FMODSound(void)
	{
		if (operator bool())
		{
			FMOD::Sound * const sound = _sound;
			_sound = 0;
			sound->release();
		}
	}
};

void WriteHeader(std::fstream & file, FMOD::Sound * sound, int length)
{
	file.seekp(0);
	float frequency = 0.f;
	int channels = 0;
	int bits = 0;
	sound->getDefaults(&frequency, 0, 0, 0);
	sound->getFormat(0, 0, &channels, &bits);
	{
		struct chunk_t { char id[4]; int size; };

		struct data_t { chunk_t head; } data = { { { 'd', 'a', 't', 'a' }, length } };

		bool extended = ((int)frequency > 44100) || (channels > 2) || ((bits != 8) && (bits != 16));

		if (channels == 1)
		{
			frequency = 88200.f;
		}
		struct fmt_t { chunk_t head; WAVEFORMATPCMEX pcm; } fmt = { { 'f', 'm', 't', ' ' } };
		fmt.pcm.Format.wFormatTag = extended ? WAVE_FORMAT_EXTENSIBLE : WAVE_FORMAT_PCM;
		fmt.pcm.Format.nChannels = channels;
		fmt.pcm.Format.nSamplesPerSec = (int)frequency;
		fmt.pcm.Format.wBitsPerSample = (bits + 7) / 8 * 8;
		fmt.pcm.Format.nBlockAlign = fmt.pcm.Format.nChannels * fmt.pcm.Format.wBitsPerSample / 8;
		fmt.pcm.Format.nAvgBytesPerSec = fmt.pcm.Format.nSamplesPerSec * fmt.pcm.Format.nBlockAlign;



		if (extended)
		{
			fmt.pcm.Format.cbSize = sizeof(fmt.pcm) - sizeof(fmt.pcm.Format);
			fmt.pcm.Samples.wValidBitsPerSample = bits;
			for (fmt.pcm.dwChannelMask = 0; channels; --channels)
				fmt.pcm.dwChannelMask = (fmt.pcm.dwChannelMask << 1) | 1;
			fmt.pcm.SubFormat = KSDATAFORMAT_SUBTYPE_PCM;
			fmt.head.size = sizeof(WAVEFORMATPCMEX);
		}
		else
		{
			fmt.head.size = sizeof(PCMWAVEFORMAT);
		}

		struct riff_t { chunk_t head; char type[4]; } riff = { { { 'R', 'I', 'F', 'F' },
			fmt.head.size + data.head.size + 2 * static_cast<int>(sizeof(chunk_t)) }, { 'W', 'A', 'V', 'E' } };

		file.write((char *)&riff, sizeof(riff_t));
		file.write((char *)&fmt, sizeof(chunk_t) + fmt.head.size);
		file.write((char *)&data, sizeof(data_t));
	}
}

FMOD_RESULT DumpSoundBank(FMODSystem & system, std::string const & filename, bool extract)
{
	std::cout << filename << std::endl;

	FMODSound sound(system, filename.c_str(), FMOD_SOFTWARE | FMOD_CREATESTREAM | FMOD_ACCURATETIME);
	FMOD_RESULT result = sound;
	if (!sound)
	{
		std::cerr <<
			"  Failed to create FMOD::Sound (" << result << ") " <<
			FMOD_ErrorString(result) << std::endl;
	}
	else
	{
		int numsubsounds = 0;
		result = sound->getNumSubSounds(&numsubsounds);
		if (result != FMOD_OK)
		{
			std::cerr <<
				"  Failed to get the number of sub sounds (" << result << ") " <<
				FMOD_ErrorString(result) << std::endl;
		}
		else
		{
			std::streamsize width = 1;
			for (int num = numsubsounds; num >= 10; num /= 10)
				++width;
			for (int index = 0; index < numsubsounds; ++index)
			{
				FMOD::Sound * subsound;
				if (FMOD_OK == sound->getSubSound(index, &subsound))
				{
					char name[0x100];
					if ((FMOD_OK == subsound->getName(name, sizeof(name) / sizeof(name[0]))) && (name[0] != 0))
					{
						std::cout << "  " << std::setw(width) << std::setfill('0') << index << " - " << name << std::endl;
						if (extract)
						{
							std::stringstream subfilename;
							subfilename << filename << "." << std::setw(width) << std::setfill('0') << index << "." << name << ".wav";
							subsound->seekData(0);
							std::fstream file(subfilename.str().c_str(), std::ios::in | std::ios::out | std::ios::binary | std::ios::trunc, _SH_DENYWR);
							if (file.is_open())
							{
								unsigned int length = 0;
								int channels = 1;
								subsound->getLength(&length, FMOD_TIMEUNIT_PCMBYTES);
								subsound->getFormat(0, 0, &channels, 0);
								if (channels == 1)
								{
									length = length * 2;
								}
								WriteHeader(file, subsound, length);
								do
								{
									static char buffer[0x10000];
									unsigned int read = (length > sizeof(buffer)) ? sizeof(buffer) : length;
									result = subsound->readData(&buffer[0], read, &read);
									file.write(&buffer[0], read);
									length -= read;
								} while (length && FMOD_OK == result);
							}
						}
					}
				}
			}
		}
	}
	return result;
}

enum command_t
{
	command_none,
	command_list,
	command_extract
};

command_t parse(int argc, char * argv[])
{
	if (2 == argc)
		return command_extract;
	if (argc != 3)
		return command_none;
	{
		char const * c = argv[1];
		switch (c[0])
		{
		case '-':
		case '/':
			++c;
			break;
		}
		switch (c[0])
		{
		case 'l':
			return command_list;
		case 'x':
			return command_extract;
		default:
			return command_none;
		}
	}
}

int main(int argc, char * argv[])
{
	FMOD_RESULT result;
	command_t command = parse(argc, argv);
	if (command_none == command)
	{
		std::cout << std::endl;
		std::cout << "fmod_towav, extract fsb files" << std::endl;
		std::cout << std::endl;
		std::cout << "Usage: fmod_towav.exe <command> <filepath>" << std::endl;
		std::cout << std::endl;
		std::cout << "Commands:" << std::endl;
		std::cout << "    l   list" << std::endl;
		std::cout << "    x   extract" << std::endl;
		result = (argc > 1) ? FMOD_ERR_BADCOMMAND : FMOD_OK;
	}
	else
	{
		FMODSystem system;
		result = system;
		if (!system)
		{
			std::cerr <<
				"  Failed to create FMOD::System (" << result << ") " <<
				FMOD_ErrorString(result) << std::endl;
		}
		else
		{
			result = system->init(16, FMOD_DEFAULT, 0);
			if (result != FMOD_OK)
			{
				std::cerr <<
					"  Failed to initialize FMOD::System (" << result << ") " <<
					FMOD_ErrorString(result) << std::endl;
			}
			else
			{
				std::string const filename(argv[argc - 1]);
				result = DumpSoundBank(system, filename, command_extract == command);
			}
		}
	}
	return result;
}
